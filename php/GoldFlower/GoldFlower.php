<?php
// 炸金花逻辑  未完。。。。。。
class GoldFlower {

	const BAOZI = 6; // 豹子
	const TONGHUASHUN = 5; // 同花顺
	const TONGHUA = 4; // 同花
	const SHUNZI = 3; // 顺子
	const DUIZI = 2; // 对子
	const DANZHANG = 1; // 单张


	public $winner;
	
	function __construct()
	{
	}
	// @param array plyaers 玩家的出牌 格式为二位数组，每个元素三个数值
	public function checkWinner($cards)
	{
		if(count($cards) == 0) return false;
		usort($cards , ['GoldFlower','sortFuncDesc']);
		$this->winner = $cards[0];
	}
	public function sortFuncDesc($one,$two)
	{
		return $this->sortFunc($one,$two,true);
	}
	public function sortFuncAsc($one,$two)
	{
		return $this->sortFunc($one,$two);
	}
	// 自定义排序函数 默认正序
	private function sortFunc($one,$two,$desc = false)
	{
		$card_one = $this->getCardType($one);

		// var_export($card_one);die;
		$card_two = $this->getCardType($two);
		if($desc == false){
			$i = 1; $j = -1;
		} else {
			$i = -1; $j = 1;
		}

		// 先比较牌面类型
		if($card_one['type'] > $card_two['type']){
			return $i;
		} elseif ($card_one['type'] == $card_two['type']) {
			// 比较值
			switch ($card_one['type']) {
				case self::BAOZI: // 爆子牌的比较
					if($card_one['value']['big'] > $card_two['value']['big'])
						$bigger = $one;
					else
						$bigger = $two;
					break;

				case self::DUIZI: // 对子牌的比较
					if($card_one['value']['big'] > $card_two['value']['big'])
						$bigger = $one;
					elseif ($card_one['value']['big'] == $card_two['value']['big']) {
						if($card_one['value']['medium'] > $card_two['value']['medium'])
							$bigger = $one;
						else
							$bigger = $two;
					}
					else $bigger = $two;
					break;

				default: // 默认是单张牌的比较
				
					if($card_one['value']['big'] > $card_two['value']['big'])
						$bigger = $one;
					elseif ($card_one['value']['big'] == $card_two['value']['big']) {
						if($card_one['value']['medium'] > $card_two['value']['medium']){
							$bigger = $one;
						} elseif ($card_one['value']['medium'] == $card_two['value']['medium']) {
							if($card_one['value']['small'] > $card_two['value']['$small'])
								$bigger = $one;
							else
								$bigger = $two;
						}
						else $bigger = $two;
					}
					else $bigger = $two;
					break;
			}
			return $bigger == $one ? $i : $j;
		} else {
			return $j;
		}
		return 0; // 如果是0就报错，不可能有牌面一样的
	}
	// 出牌结果的类型与结果
	// @parame array cards 三张牌面
	public function getCardType($cards)
	{
		if(count($cards) !== 3) 
			return false;
		sort($cards); // 把牌面从小到大排序
		$res = array_unique($cards); // 取唯一

		if(count($res) == 1){
			$type = self::BAOZI;
			$value['big'] = $res[0];
		
		} elseif (count($res) == 2){
			$type = self::DUIZI;
			$value['big'] = $res[1];
			$value['medium'] = $res[0];
		} else {
			$type = self::DANZHANG;
			$value['big'] = $res[2];
			$value['medium '] = $res[1];
			$value['small'] = $res[0];
		}
		return compact('type','value');
	}

}



?>